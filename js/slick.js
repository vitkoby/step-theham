$(document).ready(function () {
	$('.slider').slick({
		slidesToShow: 3,
		variableWidth: true,
		asNavFor: '.slider-big',
		focusOnSelect: true,
		easing: 'ease-in-out',
		swipeToSlide: true,
	});

	$('.slider-big').slick({
		slidesToShow: 1,
		arrows: false,
		asNavFor: '.slider',
		fade: true,
		
	});
});